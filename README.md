Symfony Proyect MPWAR
========================

This is a simple Symfony project about all learned in class.

What's inside?
--------------

This project have three bundles:

  * CoreBundle: They have all the logic, entities, form templates, event listeners and dispatchers, 
                other packages communicate with this using events.


  * BackBundle: Like a CMS, this simple bundle have a responsibility to launch events about
                create new category, new product, delete category or product and delete them.

  * FrontBundle: This bundle is used to present the products to the users, have a contact form
                 and launch a event when a form is submitted.

Services
--------

This project have some services to manage the content of the page, 
all service use Doctrine to make the logic in database, using the interfaces this ORM can be change:
    
  ### Category 
  ####service: category.manager.service
  #####methods:
  * save: save or update a category in database.
  * delete: delete a category from database.
  * search: get a category from database using the id to search them.
  * all: get all categories from database.
  
  ### Product 
  ####service: product.manager.service
  #####methods:
  * save: save or update a product in database.
  * delete: delete a product from database.
  * search: get a product from database using the id to search them.
  * all: get all products from database.
  * getProductByCategory: Using a category search all the products that belong to this category.
  
  ### Messages 
  ####service: message.manager.service
  #####methods:
  * save: save a message in database.
  * delete: delete a message from database.
  * search: get a message from database using the id to search them.
  * all: get all massages from database.
  
Events and Events Dispatchers
-----------------------------

This project use events to communicate to CoreBundle when a bundle needs to make a change in database,
the methods to use in events dispatcher are:

  ### Category 
  ####service: category.event.dispatcher.service
  #####methods:
  * generateNewCategoryEvent: Generate an event to create a new Category using the data matrix obtained 
  from the form submission by the POST method.
  * generateUpdateCategoryEvent: Generate an event to update a Category, it use as parameter a Category item. 
  * generateDeleteCategoryEvent: Generate an event to delete a Category, it use as parameter a Category item.
  
  ### Product 
  ####service: product.event.dispatcher.service
  #####methods:
  * generateNewProductEvent: Generate an event to create a new Product using the data matrix obtained 
  from the form submission by the POST method.
  * generateUpdateProductEvent: Generate an event to update a Product, it use as parameter a Product item. 
  * generateDeleteProductEvent: Generate an event to delete a Product, it use as parameter a Product item.

  ### Message 
  ####service: message.event.dispatcher.service
  #####methods:
  * generateNewMessageEvent: Generate an event to create a new Message using the data matrix obtained 
  from the form submission by the POST method.
  * generateDeleteMessageEvent: Generate an event to delete a Message, it use as parameter a Contact item.


Installation
------------

To install this project you need machine with [**PHP 7**][1] and [**MySQL**][2], libraries PHP like php-xml and php-mysql to make Symfony 
run, and [**Composer**][3] to install dependencies to the project.
  
  * Clone or Download the project 
  * In the path of project run 'composer update'
  * When the installation of composer finish you can configure the database parameters like host, username and password
  
  After the installation you will need create the database and tables for the project, then, go to project's path 
  and using the command console run:
  
  * php bin/console doctrine:database:create, this command will create a database where the project save data
  * php bin/console doctrine:schema:update, this command will create the database schema to the project, 
  all tables and relations

[1]: http://askubuntu.com/questions/856793/upgrade-to-the-specific-php-7-1-from-php-7-0-in-ubuntu-16-04
[2]: https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-16-04
[3]: https://getcomposer.org/
