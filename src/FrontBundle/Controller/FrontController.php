<?php

namespace FrontBundle\Controller;

use FrontBundle\Form\ContactForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class FrontController extends Controller
{

    public function indexAction()
    {
        $products = $this->get('product.manager.service')->all();

        return $this->render('FrontBundle:Default:products.html.twig', [
            'products' => $products,
            'title' => 'Productos'
        ]);
    }

    public function itemShowAction(int $id)
    {
        $product = $this->get('product.manager.service')->search($id);

        return $this->render('FrontBundle:Default:item.html.twig', ['product' => $product]);
    }

    public function categoryShowAction()
    {
        $categories = $this->get('category.manager.service')->all();

        return $this->render('FrontBundle:Default:category.html.twig', ['categories' => $categories]);
    }

    public function productByCategoryShowAction(int $id)
    {
        $category = $this->get('category.manager.service')->search($id);

        $products = $this->get('product.manager.service')->getProductByCategory($category);

        return $this->render('FrontBundle:Default:products.html.twig', [
            'products' => $products,
            'title' => $category->getName()
        ]);
    }

    public function contactFormAction(Request $request)
    {
        $form = $this->createForm(ContactForm::class, null, ['label' => 'Enviar']);

        $form->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            if ($form->isSubmitted() && $form->isValid()) {
                $this->get('message.event.dispatcher.service')->generateNewMessageEvent($form->getData());

                return $this->redirectToRoute('homepage');
            }
        }

        return $this->render('FrontBundle:Default:contact.html.twig', ['form' => $form->createView()]);
    }

}