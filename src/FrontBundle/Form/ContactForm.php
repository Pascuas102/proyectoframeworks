<?php

namespace FrontBundle\Form;

use CoreBundle\Form\ContactType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ContactForm extends ContactType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('save', SubmitType::class, ['label' => $options['label']]);
    }

}