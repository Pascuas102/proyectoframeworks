<?php

namespace BackBundle\Controller;

use BackBundle\Form\CategoryFrom;
use BackBundle\Form\ProductForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BackController extends Controller
{

    public function indexAction()
    {
        $products = $this->get('product.manager.service')->all();

        return $this->render('BackBundle:Default:index.html.twig', ['products' => $products]);
    }

    public function createCategoryAction(Request $request)
    {
        $categories = $this->get('category.manager.service')->all();

        $form = $this->createForm(CategoryFrom::class, null, ['label' => 'Crear Categoria']);

        $form->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            if ($form->isSubmitted() && $form->isValid()) {
                $this->get('category.event.dispatcher.service')->generateNewCategoryEvent($form->getData());

                return $this->redirectToRoute('create_category');
            }
        }

        return $this->render('BackBundle:Default:manage_category.html.twig', [
            'form' => $form->createView(),
            'categories' => $categories
        ]);
    }

    public function updateCategoryAction(Request $request, $id)
    {

        $category = $this->get('category.manager.service')->search($id);

        $form = $this->createForm(CategoryFrom::class, $category, ['label' => 'Actualizar Categoria']);

        $form->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            if ($form->isSubmitted() && $form->isValid()) {
                $this->get('category.event.dispatcher.service')->generateUpdateCategoryEvent($category);

                return $this->redirectToRoute('create_category');
            }
        }

        return $this->render('BackBundle:Default:manage_category.html.twig', [
            'form' => $form->createView(),
            'categories' => false
        ]);
    }

    public function deleteCategoryAction($id)
    {
        $category = $this->get('category.manager.service')->search($id);
        $this->get('category.event.dispatcher.service')->generateDeleteCategoryEvent($category);

        return $this->redirectToRoute('create_category');
    }

    public function createProductAction(Request $request)
    {
        $form = $this->createForm(ProductForm::class, null, ['label' => 'Agregar Producto']);

        $form->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            if ($form->isSubmitted() && $form->isValid()) {
                $this->get('product.event.dispatcher.service')->generateNewProductEvent($form->getData());

                return $this->redirectToRoute('manager');
            }
        }

        return $this->render('BackBundle:Default:manage_product.html.twig', [
            'form' => $form->createView(),
            'title' => 'Crear Producto'
        ]);
    }

    public function updateProductAction(Request $request, $id)
    {
        $product = $this->get('product.manager.service')->search($id);

        $form = $this->createForm(ProductForm::class, $product, ['label' => 'Editar Producto']);

        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            if ($form->isSubmitted() && $form->isValid()) {
                $this->get('product.event.dispatcher.service')->generateUpdateProductEvent($product);

                return $this->redirectToRoute('manager');
            }
        }

        return $this->render('BackBundle:Default:manage_product.html.twig', [
            'form' => $form->createView(),
            'title' => 'Editar Producto'
        ]);
    }

    public function deleteProductAction($id)
    {
        $product = $this->get('product.manager.service')->search($id);
        $this->get('product.event.dispatcher.service')->generateDeleteProductEvent($product);

        return $this->redirectToRoute('manager');
    }

    public function showMessagesAction()
    {
        $messages = $this->get('message.manager.service')->all();

        return $this->render('BackBundle:Default:messages.html.twig', ['messages' => $messages]);
    }

    public function deleteMessageAction($id)
    {
        $message = $this->get('message.manager.service')->search($id);
        $this->get('message.event.dispatcher.service')->generateDeleteMessageEvent($message);

        return $this->redirectToRoute('received_messages');
    }
}