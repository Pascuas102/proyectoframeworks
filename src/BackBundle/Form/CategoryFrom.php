<?php

namespace BackBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use CoreBundle\Form\CategoryType;

class CategoryFrom extends CategoryType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('save',SubmitType::class, ['label' => $options['label']]);
    }
}