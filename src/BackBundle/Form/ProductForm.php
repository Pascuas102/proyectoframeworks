<?php

namespace BackBundle\Form;

use CoreBundle\Form\ProductType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


class ProductForm extends ProductType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);

        $builder->add('save', SubmitType::class, ['label' => $options['label'],
            'attr' => ['class' => 'btn btn-default']]
        );
    }
}
