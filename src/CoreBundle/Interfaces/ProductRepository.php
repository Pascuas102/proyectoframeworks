<?php

namespace CoreBundle\Interfaces;

use CoreBundle\Entity\Product;

interface ProductRepository
{
    public function save(Product $product);
    public function delete(Product $product);
    public function search(int $id);
    public function all();

}