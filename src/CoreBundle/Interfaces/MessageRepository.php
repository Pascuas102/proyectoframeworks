<?php

namespace CoreBundle\Interfaces;

use CoreBundle\Entity\Contact;

interface MessageRepository
{
    public function save(Contact $message);
    public function delete(Contact $message);
    public function search(int $id);
    public function all();
}