<?php

namespace CoreBundle\Interfaces;

use CoreBundle\Entity\Category;

interface CategoryRepository
{
    public function save(Category $category);
    public function delete(Category $category);
    public function search(int $id);
    public function all();
}