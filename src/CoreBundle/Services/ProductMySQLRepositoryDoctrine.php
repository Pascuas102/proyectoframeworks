<?php

namespace CoreBundle\Services;

use CoreBundle\Entity\Product;
use CoreBundle\Interfaces\ProductRepository;
use Doctrine\ORM\EntityManager;
use CoreBundle\Entity\Category;

class ProductMySQLRepositoryDoctrine implements ProductRepository
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->getRepository();
    }

    public function save(Product $product)
    {
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }

    public function delete(Product $product)
    {
        $this->entityManager->remove($product);
        $this->entityManager->flush();
    }

    public function all()
    {
        $query = $this->entityManager->createQueryBuilder()
            ->select('p','c')
            ->from('CoreBundle\Entity\Product', 'p')
            ->leftJoin('p.category', 'c');

        return $query->getQuery()->getResult();
    }

    public function search(int $id)
    {
        $query = $this->entityManager->createQueryBuilder()
            ->select('p','c')
            ->from('CoreBundle\Entity\Product', 'p')
            ->leftJoin('p.category', 'c')
            ->where('p.id = :id')
            ->setParameter('id', $id);

        return $query->getQuery()->getOneOrNullResult();
    }

    public function getProductByCategory(Category $category)
    {
        $productContainer = $this->getRepository();
        return $productContainer->findBy(['category' => $category]);
    }

    private function getRepository()
    {
        return $this->entityManager->getRepository('CoreBundle:Product');
    }
}