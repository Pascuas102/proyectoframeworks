<?php

namespace CoreBundle\Services;

use CoreBundle\Entity\Contact;
use CoreBundle\Interfaces\MessageRepository;
use Doctrine\ORM\EntityManager;

class MessageMySQLRepositoryDoctrine implements MessageRepository
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->getRepository();
    }

    function save(Contact $message)
    {
        $this->entityManager->persist($message);
        $this->entityManager->flush();

    }

    function delete(Contact $message)
    {
        $this->entityManager->remove($message);
        $this->entityManager->flush();
    }

    public function all()
    {
        $messageContainer = $this->getRepository();
        return $messageContainer->findAll();
    }

    public function search(int $id)
    {
        $messageContainer = $this->getRepository();
        return $messageContainer->find($id);
    }

    private function getRepository()
    {
        return $this->entityManager->getRepository('CoreBundle:Contact');
    }

}