<?php

namespace CoreBundle\Services;

use CoreBundle\Entity\Category;
use CoreBundle\Event\CategoryEvent;

class CategoryEventDispatcher
{
    private $event;

    public function __construct($event)
    {
        $this->event = $event;
    }

    public function generateNewCategoryEvent($data)
    {
        $category = new Category();
        $category->setName($data['name']);
        $category->setImageUrl($data['image_url']);
        $category = new CategoryEvent($category);

        $this->event->dispatch('create_category', $category);
    }

    public function generateUpdateCategoryEvent(Category $category)
    {
        $category = new CategoryEvent($category);
        $this->event->dispatch('update_category', $category);
    }

    public function generateDeleteCategoryEvent(Category $category)
    {
        $category = new CategoryEvent($category);

        $this->event->dispatch('delete_category', $category);
    }

}