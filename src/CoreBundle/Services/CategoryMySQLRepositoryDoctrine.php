<?php

namespace CoreBundle\Services;

use CoreBundle\Entity\Category;
use Doctrine\ORM\EntityManager;
use CoreBundle\Interfaces\CategoryRepository;

class CategoryMySQLRepositoryDoctrine implements CategoryRepository
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->getRepository();
    }

    public function save(Category $category)
    {
        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }

    public function delete(Category $category)
    {
        $this->entityManager->remove($category);
        $this->entityManager->flush();
    }

    public function all()
    {
        $categoryContainer = $this->getRepository();
        return $categoryContainer->findAll();
    }

    public function search(int $id)
    {
        return $this->getRepository()->find($id);
    }

    private function getRepository()
    {
        return $this->entityManager->getRepository('CoreBundle:Category');
    }
}