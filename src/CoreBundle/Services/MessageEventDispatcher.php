<?php

namespace CoreBundle\Services;

use CoreBundle\Entity\Contact;
use CoreBundle\Event\MessageEvent;

class MessageEventDispatcher
{
    private $event;

    public function __construct($event)
    {
        $this->event = $event;
    }

    public function generateNewMessageEvent($data)
    {
        $message = new Contact();
        $message->setName($data['name']);
        $message->setContact($data['contact']);
        $message->setSubject($data['subject']);
        $message->setMessage($data['message']);
        $message = new MessageEvent($message);

        $this->event->dispatch('send_message', $message);
    }

    public function generateDeleteMessageEvent(Contact $message)
    {
        $message = new MessageEvent($message);

        $this->event->dispatch('delete_message', $message);
    }

}