<?php

namespace CoreBundle\Services;

use CoreBundle\Entity\Product;
use CoreBundle\Event\ProductEvent;

class ProductEventDispatcher
{
    private $event;

    public function __construct($event)
    {
        $this->event = $event;
    }

    public function generateNewProductEvent($data)
    {
        $product = new Product();
        $product->setSku($data['sku']);
        $product->setName($data['name']);
        $product->setPrice($data['price']);
        $product->setDescription($data['description']);
        $product->setUrl($data['url']);
        $product->setCategory($data['category']);
        $product = new ProductEvent($product);

        $this->event->dispatch('create_product', $product);
    }

    public function generateUpdateProductEvent(Product $product)
    {
        $product = new ProductEvent($product);
        $this->event->dispatch('update_product', $product);
    }

    public function generateDeleteProductEvent(Product $product)
    {
        $product = new ProductEvent($product);

        $this->event->dispatch('delete_product', $product);
    }

}