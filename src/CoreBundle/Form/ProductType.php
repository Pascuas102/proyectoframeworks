<?php

namespace CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sku', TextType::class)
                ->add('name', TextType::class)
                ->add('price', TextType::class)
                ->add('description', TextareaType::class)
                ->add('url', UrlType::class)
                ->add('category', EntityType::class, [
                    'class' => 'CoreBundle:Category',
                    'choice_label' => 'name',
                    'label' => 'Categoria',
                ]);
    }
}
