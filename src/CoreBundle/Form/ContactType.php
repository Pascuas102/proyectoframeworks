<?php

namespace CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, ['label' => 'Nombre', 'attr' => ['class' => 'form-control']])
            ->add('contact', EmailType::class, ['label' => 'Email', 'attr' => ['class' => 'form-control']])
            ->add('subject', TextType::class, ['label' => 'Asunto', 'attr' => ['class' => 'form-control']])
            ->add('message', TextareaType::class, ['label' => 'Mensaje', 'attr' => ['class' => 'form-control']]);
    }

}