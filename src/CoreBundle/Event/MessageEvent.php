<?php

namespace CoreBundle\Event;

use CoreBundle\Entity\Contact;
use Symfony\Component\EventDispatcher\Event;

class MessageEvent extends Event
{
    private $message;

    public function __construct(Contact $message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }

}