<?php

namespace CoreBundle\EventListener;

use CoreBundle\Interfaces\CategoryRepository;
use Symfony\Component\EventDispatcher\Event;

class CategoryManagerEventListener
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function createCategory(Event $event)
    {
        $this->categoryRepository->save($event->getCategory());
    }

    public function updateCategory(Event $event)
    {
        $this->categoryRepository->save($event->getCategory());
    }

    public function deleteCategory(Event $event)
    {
        $this->categoryRepository->delete($event->getCategory());
    }

}