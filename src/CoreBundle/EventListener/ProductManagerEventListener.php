<?php

namespace CoreBundle\EventListener;

use CoreBundle\Services\ProductMySQLRepositoryDoctrine;
use Symfony\Component\EventDispatcher\Event;

class ProductManagerEventListener
{
    private $productManager;

    public function __construct(ProductMySQLRepositoryDoctrine $productManager)
    {
        $this->productManager = $productManager;
    }

    public function createProduct(Event $event)
    {
        $this->productManager->save($event->getProduct());
    }

    public function updateProduct(Event $event)
    {
        $this->productManager->save($event->getProduct());
    }

    public function deleteProduct(Event $event)
    {
        $this->productManager->delete($event->getProduct());
    }

}