<?php

namespace CoreBundle\EventListener;

use CoreBundle\Services\MessageMySQLRepositoryDoctrine;
use Symfony\Component\EventDispatcher\Event;

class MessageManagerEventListener
{
    private $messageManager;

    public function __construct(MessageMySQLRepositoryDoctrine $messageManager)
    {
        $this->messageManager = $messageManager;
    }

    public function createMessage(Event $event)
    {
        $this->messageManager->save($event->getMessage());
    }

    public function deleteMessage(Event $event)
    {
        $this->messageManager->delete($event->getMessage());
    }
}